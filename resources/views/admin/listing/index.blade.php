@extends('layouts.app')
 
@section('content')
 
    <div class="flex-center position-ref full-height">
 
        <div class="content">
            <h2 class="title m-b-md" style="background: #ccc">
                Listings
            </h2>
 
            <div class="row">
                @foreach($listings as $listing)
                    <article class="col-md-10">
                        <h4><a href="{{ url('listings/' . $listing->id) }}">{{ $listing->title }}</a></h4>
                        <img src="{{ $listing->image }}" width="250" class="pull-left img-responsive thumb img-thumbnail" />
                        <p>
                            {{ substr($listing->description, 0, 50) }}
                        </p>
                    </article>
                    <hr/>
                @endforeach
            </div>
        </div>
    </div>
 
@stop