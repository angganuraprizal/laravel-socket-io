@extends('layouts.app')
 
@section('content')
 
    <div class="flex-center position-ref full-height">
 
        <div class="content">
            <h1 class="title m-b-md" style="background: #ccc">
                {{ $listing->title }}
            </h1>
            <span><strong>Price: </strong> <i class="label label-success">$ {{ number_format($listing->price, 1) }}</i></span>
            <div class="row">
                <article class="col-md-12">
                    <img src="{{ $listing->image }}" class="img-responsive thumb img-thumbnail" />
                    <p>
                        {{ $listing->description }}
                    </p>
                </article>
            </div>
        </div>
    </div>
 
@stop

@section('scripts')
 
    @if(\Auth::check())
 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
 
        <script src="{{ url('/js/laravel-echo-setup.js') }}" type="text/javascript"></script>
 
        <script>
            window.Echo.channel('listings.{{$listing->id}}')
                .listen('ListingViewed', function (e) {
 
                    if(e.data.current_user != parseInt('{{ \Auth::user()->id }}')) {
 
                        showNotification("Another user looking at this listing right now");
                    }
 
                });
 
            function showNotification(msg) {
 
                if (!("Notification" in window)) {
                    alert("This browser does not support desktop notification");
                }
 
                else if (Notification.permission === "granted") {
                    // If it's okay let's create a notification
                    var notification = new Notification(msg);
                }
 
                else if (Notification.permission !== "denied") {
                    Notification.requestPermission().then(function (permission) {
                        // If the user accepts, let's create a notification
                        if (permission === "granted") {
                            var notification = new Notification(msg);
                        }
                    });
                }
            }
        </script>
 
    @endif
 
@stop