<?php

namespace App\Http\Controllers\Admin;

use App\Events\ListingViewed;
use App\Models\Listing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListingController extends Controller
{
    public function index()
    {
        $listings = Listing::all();
 
        return view('admin.listing.index')->with('listings', $listings);
    }
 
    public function show($id)
    {
        $listing = Listing::find($id);

        if(Auth::check()) {
            event(new ListingViewed($listing)); // fire the event
        }
 
        return view('admin.listing.show')->with('listing', $listing);
    }
}
